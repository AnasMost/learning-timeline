const targets = document.querySelector('#timeline > .container').children

const throttleInterval = 400
const visibilityRatio = 1
const windowThresholdRatio = 0.5

// Add Features to HTMLElement Constructor
class Item {

  constructor(element) {
    this.element = element
  }

  show() {
    this.element.classList.remove('hidden')
    this.element.classList.add('visible')
  }

  hide() {
    this.element.classList.remove('visible')
    this.element.classList.add('hidden')
  }

  isHidden() {
    return this.element.style.opacity == 0
  }

  isVisibleY(visibilityRatio, windowThresholdRatio) {
    const position = this.element.getBoundingClientRect()

    const threshold = Math.min(visibilityRatio * position.height, windowThresholdRatio * window.innerHeight)

    return window.innerHeight > position.top + threshold
  }
}

// Scroll Handler Feature
class ScrollHandler {

  constructor(targets, visibilityRatio, windowThresholdRatio, throttleInterval) {
    this.targets = Array.from(targets).map(item => new Item(item))
    this.VR = visibilityRatio
    this.WTR = windowThresholdRatio
    this.TI = throttleInterval
    this.init()
    this.Events()
  }

  // Events
  Events() {
    window.addEventListener('scroll', this.run.bind(this))
    window.addEventListener('resize', this.run.bind(this))
    window.addEventListener('load', this.run.bind(this))
  }

  // Hide invisible Items
  init() {
    this.targets.forEach(item => {
      if (!item.isVisibleY(this.VR, this.WTR)) {
        item.hide()
      }
    })
  }

  // Main Runing Function
  run() {
    if (this.targets.length > 0) {
      let fn = this.throttle(this.TI, this.showScrolled)
      if (fn !== null) { fn.call(this) }
    }
  }

  showScrolled() {
    for (let i = 0, n = this.targets.length; i < n; i++) {
      if (this.targets[i].isHidden() && this.targets[i].isVisibleY(this.VR, this.WTR)) {
        this.targets[i].show()
        this.targets.splice(i, 1)
        i--; n--
      }
    }
  }
  // Feature
  throttle(delay, fn) {
    const now = Date.now()
    let remainingTime = (fn.lastCall || -Infinity) + delay - now;

    return remainingTime > 0 ?
      null :
      function () {
        clearTimeout(fn.timer)
        fn.call(this)
        fn.lastCall = now
        fn.timer = setTimeout(fn.bind(this), delay)
      }
  }
}

new ScrollHandler(targets, visibilityRatio, windowThresholdRatio, throttleInterval)